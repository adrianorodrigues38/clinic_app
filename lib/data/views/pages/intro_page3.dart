import 'package:clinic_app/data/views/pages/widget_page.dart';
import 'package:flutter/material.dart';

class IntroPage3 extends StatelessWidget {
  const IntroPage3({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.pink[100],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildPage(
              color: Colors.white,
              urlImage: 'assets/images/hospital.jpg',
              title: 'REDUCE',
              subtitle:
                  'You dont have to go far to find a good restaurant,we have provided all the restaurants that near you',
            ),
          ],
          // child: Text('Page One'),
        ),
      ),
    );
  }
}
