import 'package:clinic_app/data/views/pages/widget_page.dart';
import 'package:flutter/material.dart';

class IntroPage2 extends StatelessWidget {
  const IntroPage2({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.purple[100],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildPage(
              color: Colors.white,
              //C:\Users\lucas\OneDrive\Documentos\APP\clinic_app\assets\images
              urlImage: 'assets/images/hospital1.jpg',
              title: 'TEXTO AQUI!',
              subtitle:
                  'You dont have to go far to find a good restaurant,we have provided all the restaurants that near you',
            ),
          ],
          // child: Text('Page One'),
        ),
      ),
    );
  }
}
