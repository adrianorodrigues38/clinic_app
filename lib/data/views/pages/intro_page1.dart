import 'package:clinic_app/data/views/pages/widget_page.dart';
import 'package:flutter/material.dart';

class IntroPage1 extends StatelessWidget {
  const IntroPage1({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.lightGreen[100],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          buildPage(
            color: Colors.white,
            urlImage: 'assets/images/doctor4.jpg',
            title: 'TEXTO AQUI!',
            subtitle:
                'You dont have to go far to find a good restaurant,we have provided all the restaurants that near you',
          ),
        ],
        // child: Text('Page One'),
      ),
    );
  }
}
