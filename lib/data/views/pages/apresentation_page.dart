import 'package:clinic_app/data/views/pages/home_page.dart';
import 'package:clinic_app/data/views/pages/intro_page1.dart';
import 'package:clinic_app/data/views/pages/intro_page2.dart';
import 'package:clinic_app/data/views/pages/intro_page3.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class Apresentation extends StatefulWidget {
  const Apresentation({super.key});

  @override
  State<Apresentation> createState() => _ApresentationState();
}

class _ApresentationState extends State<Apresentation> {
  PageController _controller = PageController();

  bool onLastPage = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            controller: _controller,
            onPageChanged: (index) {
              setState(() {
                onLastPage = (index == 2);
              });
            },
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              // ignore: prefer_const_constructors
              IntroPage1(),
              // ignore: prefer_const_constructors
              IntroPage2(),
              // ignore: prefer_const_constructors
              IntroPage3(),
            ],
          ),
          Container(
            alignment: Alignment(0, 0.75),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //skip
                GestureDetector(
                  onTap: () {
                    _controller.jumpToPage(2);
                  },
                  child: Text('skip'),
                ),
                SmoothPageIndicator(
                  controller: _controller,
                  count: 3,
                  onDotClicked: (index) => _controller.animateToPage(
                    index,
                    duration: const Duration(microseconds: 500),
                    curve: Curves.easeIn,
                  ),
                ),
                onLastPage
                    ?
                    //next
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return Homepage();
                              },
                            ),
                          );
                        },
                        child: Text('done'),
                      )
                    : GestureDetector(
                        onTap: () {
                          _controller.nextPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeIn,
                          );
                        },
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.green,
                          size: 24.0,
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
